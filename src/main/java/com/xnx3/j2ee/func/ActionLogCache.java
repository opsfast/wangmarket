package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.ActionLogUtil;

/**
 * 会员动作日志的缓存及使用。
 * 有其他日志需要记录，可以参考这个类。可吧这个类复制出来，在此基础上进行修改
 * @author 管雷鸣
 * @deprecated 已废弃！！ 请使用 {@link ActionLogUtil}
 */
public class ActionLogCache extends ActionLogUtil{
}
